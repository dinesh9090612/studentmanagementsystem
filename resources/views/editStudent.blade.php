<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <!-- return view('editStudent', ["student" => $query, "studentId" => $id]); -->
    <!-- return view('editStudent', ["student" => $student]); -->
    <table>

        <form action="{{ (Request::is('editStudentEloquent')) ? route('updateStudentEloquent',['id'=>$student['Sid'] ]) : route('updateStudent')}}" method="POST">
            @method('PUT')
            @csrf
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <tr>
                <td>Name</td>
                <td> <input type="text" name="update_name" id="" value=" {{(Route::is('editStudentEloquent')) ? $student['student_name']:$student[0]->student_name}}"> </td>
            </tr>
            <tr>
                <td>Gender</td>
                <td><input type="text" name="update_gender" id="" value="{{(Route::is('editStudentEloquent'))? $student['student_gender']:$student[0]->student_gender}}"> </td>
            </tr>
            <tr>
                <td> Age </td>
                <td> <input type="text" name="update_age" id="" value="{{(Route::is('editStudentEloquent'))? $student['student_age']:$student[0]->student_age}}"> </td>
            </tr>
            <tr>
                <td> School name </td>
                <td> <input type="text" name="update_school" id="" value="{{(Route::is('editStudentEloquent'))?$student['student_school']:$student[0]->student_school}}"> </td>
            </tr>
            <tr>
                <td> University </td>
                <td> <input type="text" name="update_university" id="" value="{{Route::is('editStudentEloquent')?$student['student_university']:$student[0]->student_university}}"> </td>
            </tr>
            <tr>
                <td>
                    <input type="hidden" name="hidden_stud_id" value="{{ Route::is('editStudent') ? $studentId: $student['Sid']}}">
                </td>
                <td>
                    <input type="submit" value="Update">
                </td>
            </tr>
        </form>
    </table>

</body>

</html>