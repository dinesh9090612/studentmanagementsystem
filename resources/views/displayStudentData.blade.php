<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <table>
        <tr>
            <th>Name</th>
            <th>Gender</th>
            <th>Age</th>
            <th>School</th>
            <th>University </th>
            <th>Actions</th>
        </tr>
        @foreach($allStudents as $studentData)
        <tr>
            <td>{{$studentData->student_name}}</td>
            <td>{{$studentData->student_gender}}</td>
            <td>{{$studentData->student_age}}</td>
            <td>{{$studentData->student_school}}</td>
            <td>{{$studentData->student_university}}</td>
            <td>
                <a href="{{url('editStudentEloquent/'.$studentData->Sid)}}"><input type="submit" value="Edit"></a>
                <a href="{{url('deleteStudentById/'.$studentData->Sid)}}"><input type="submit" value="Delete"></a>
            </td>
        </tr>
        @endforeach
    </table>
</body>

</html>