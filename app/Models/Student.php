<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $primaryKey = 'Sid';
    protected $table = 'Student';
    protected $fillable  = ['student_name', 'student_gender', 'student_age', 'student_school', 'student_university'];
}
