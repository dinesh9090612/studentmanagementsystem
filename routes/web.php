<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('form', [StudentController::class, 'index']);

Route::post('form', [StudentController::class, 'store']);

Route::get('success', [StudentController::class, 'successPage']);

Route::get('displayStudents', [StudentController::class, 'displayStudents']);

Route::get('edit/{studentId}', [StudentController::class, 'editStudentDetails'])->name('editStudent');

Route::put('updateStudent', [StudentController::class, 'updateStudentDetails'])->name('updateStudent');

Route::get('deleteStudent/{studentId}', [StudentController::class, 'deleteUserDetails']);

// CRUD USING ELONQUET
Route::get('getStudentDetails', [StudentController::class, 'getStudentDetails']);

Route::get('deleteStudentById/{id}', [StudentController::class, 'deleteStudentById']);

Route::get('storeEloquent', [StudentController::class, 'index']);

Route::post('storeEloquent', [StudentController::class, 'storeEloquent']);

Route::get('editStudentEloquent/{id}', [StudentController::class, 'editStudentEloquent'])->name('editStudentEloquent');

Route::put('updateStudentEloquent/{id}', [StudentController::class, 'updateStudentEloquent'])->name('updateStudentEloquent');
