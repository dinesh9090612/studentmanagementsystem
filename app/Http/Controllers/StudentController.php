<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Student;

class StudentController extends Controller
{
    public function index()
    {
        return view('form');
    }

    public function store(Request $request)
    {
        $studentData = $request->all();
        $studentName = $studentData["student_name"];
        $studentGender = $studentData["student_gender"];
        $studentAge = $studentData["student_age"];
        $studentSchool = $studentData["student_school"];
        $studentUniversity = $studentData["student_university"];
        $request->validate(
            [
                'student_name' => 'required',
                'student_gender' => 'required',
                'student_age' => 'required|integer',
                'student_school' => 'required',
                'student_university' => 'required',
            ]
        );
        DB::insert('insert into student values(?,?,?,?,?,?)', [null, $studentName, $studentGender, $studentAge, $studentSchool, $studentUniversity]);
        return redirect('success');
    }

    public function successPage()
    {
        return view('success');
    }

    public function displayStudents()
    {
        $resultSet = DB::select('SELECT * FROM STUDENT');
        return view('displayStudent', ['resultSet' => $resultSet]);
    }

    public function editStudentDetails($id)
    {
        $query = DB::select("SELECT * FROM STUDENT WHERE Sid ='" . $id . "'");
        return view('editStudent', ["student" => $query, "studentId" => $id]);
    }

    public function updateStudentDetails(Request $request)
    {
        $data = $request->all();
        DB::update("UPDATE STUDENT SET student_name=?,student_gender=?,student_age=?,student_school=?,student_university=? WHERE Sid = ?", [$data["update_name"], $data["update_gender"], $data["update_age"], $data["update_school"], $data["update_university"], $data["hidden_stud_id"]]);
        return response("Data Updated Successfully !");
    }

    public function deleteUserDetails($id)
    {
        DB::delete("DELETE FROM STUDENT WHERE Sid = ?", array($id));
        return response('Data Deleted SuccessFully ! ' . $id);
    }


    // Eloquent ORM 

    public function getStudentDetails()
    {
        $students = Student::all();
        if (count($students) > 1) {

            return view('displayStudentData', ["allStudents" => $students]);
        } else {
            return response("There is Nothing To Display !");
        }
    }

    public function deleteStudentById($id)
    {
        $student = Student::find($id);
        if (isset($student)) {
            $student->delete();
            return response("Data Deleted Successfully ! ");
        } else {
            return response("Details with Id " . $id . " Not Found");
        }
    }

    public function storeEloquent(Request $request)
    {
        Student::create($request->all());
        return response("Data Inserted Successfully !!");
    }
    public function updateEloquent(Request $reqeust, $id)

    {
        $student = Student::find($id);
        $student->student_name = $reqeust->student_name;
        $student->save();
        return response("Data Updated with Eloquate ORM !");
    }


    public function editStudentEloquent($id)
    {
        $student =  Student::find($id);
        if ($student != NULL) {
            return view('editStudent', ["student" => $student]);
        } else {
            return response("Student with ID " . $id . " Not Found");
        }
    }

    public function updateStudentEloquent(Request $request, $id)
    {
        $student = Student::find($id);
        $student['student_name'] = $request->input('update_name');
        $student['student_age'] = $request->input('update_age');
        $student['student_gender'] = $request->input('update_gender');
        $student['student_school'] = $request->input('update_school');
        $student['student_university'] = $request->input('update_university');
        $student->update();
        return response("Fields Update Successfully ");
    }
}

// indentation php
// eloquent orm
// naming conventions