<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border-collapse: collapse;

        }

        table,
        td,
        th {
            border: 1px solid;
            padding: 1rem;
        }
    </style>
</head>

<body>
    <table>
        <tr>
            <th> Student Id</th>
            <th> Student Name </th>
            <th> Student Gender </th>
            <th> Student Age </th>
            <th> School </th>
            <th> University </th>
            <th> Actions</th>
        </tr>

        @foreach($resultSet as $student)
        <tr>
            <td>{{$student->Sid}}</td>
            <td>{{$student->student_name}}</td>
            <td>{{$student->student_gender}}</td>
            <td>{{$student->student_age}}</td>
            <td>{{$student->student_school}}</td>
            <td>{{$student->student_university}}</td>
            <td>
                <a href="{{url('edit/'.$student->Sid)}}"><input type="submit" value="Edit" name="editBtn"></a>
                <a href="{{url('deleteStudent/'.$student->Sid)}}"><input type="submit" value="Delete" name="deleteBtn"></a>
            </td>
        </tr>
        @endforeach

    </table>

</body>

</html>