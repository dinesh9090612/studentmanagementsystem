<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <form action="" method="POST">
        @csrf
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <table>
            <tr>
                <td> <label for="sname"> Name </label> </td>
                <td> <input type="text" name="student_name" id="sname" value="{{old('student_name')}}"></td>
                <td>
                    @error('student_name')
                    {{$message}}
                    @enderror
                </td>
            </tr>
            <tr>
                <td> <label for="sgender"> Gender </label> </td>
                <td> <input type="text" name="student_gender" id="sgender" value="{{old('student_gender')}}"></td>
                <td>
                    @error("student_gender")
                    {{$message}}
                    @enderror
                </td>
            </tr>
            <tr>
                <td> <label for="sage"> Age </label> </td>
                <td> <input type="text" name="student_age" id="sage" value="{{old('student_age')}}"></td>
                <td>
                    @error("student_age")
                    {{$message}}
                    @enderror
                </td>
            </tr>
            <tr>

                <td> <label for="school_name"> School Name </label> </td>
                <td> <input type="text" name="student_school" id="school_name" value="{{old('student_school')}}"></td>
                <td>
                    @error("student_school")
                    {{$message}}
                    @enderror
                </td>
            </tr>
            <tr>
                <td> <label for="stud_university"> University </label> </td>
                <td> <input type="text" name="student_university" id="stud_university"></td>
                <td>

                    @error("student_university")
                    {{$message}}
                    @enderror
                </td>
            </tr>
            <tr>
                <td> <input type="submit" name="submitBtn"></td>
            </tr>

        </table>
    </form>
</body>

</html>